import axios from '../apis'

export default {
    setActivity(context, payload) {
        context.commit('SET_ACTIVITY', payload)
    },
    setNotification(context, payload) {
        let timeOut = setTimeout(function () {
            context.dispatch('clearNotification')
        }, 2000)
        context.commit('SET_NOTIFICATION', { status: payload.code, message: payload.data.message })
    },
    clearNotification(context) {
        context.commit('CLEAR_NOTIFICATION')
    },
}