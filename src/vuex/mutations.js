import types from './mutation-types'

export default {
    [types.SET_ACTIVITY] (state, payload) {
        state.activity = payload
    },
    // Set notification status and message
    [types.SET_NOTIFICATION] (state, payload) {
        const { status, message } = payload
        state.notification.status = (status !== 400) ? 'alert-success' : 'alert-danger'
        state.notification.message = message
    },
    // Clear notification status and message
    [types.CLEAR_NOTIFICATION] (state, payload) {
        state.notification.status = ''
        state.notification.message = ''
    },
}