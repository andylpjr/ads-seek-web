import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'
import getters from './getters'
import actions from './actions'

import ad from './modules/ad/store'
import promo from './modules/promo/store'
import customer from './modules/customer/store'
import checkout from './modules/checkout/store'

Vue.use(Vuex)

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions,
  modules: {
    ad,
    promo,
    customer,
    checkout
  }
})