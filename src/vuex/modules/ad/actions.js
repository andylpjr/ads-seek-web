import axios from '../../../apis'

export default {
    fetchAds(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of ads
                axios.get(payload.url)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_ADS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    fetchAdById(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of ads by Id
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_ADS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    // Create or Update ad information
    saveAds(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 201 || response.status === 200) {  
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    // Delete ad information
    deleteAd(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 200) { 
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    autocompleteAds(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SEARCH_ADS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    }
}