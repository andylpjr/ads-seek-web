import types from '../../mutation-types'

export default {
  [types.SET_ADS] (state, payload) {
    state.ads = payload
  },
  [types.SEARCH_ADS] (state, payload) {
    state.searchedAds = payload
  }
}