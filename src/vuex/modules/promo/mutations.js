import types from '../../mutation-types'

export default {
  [types.SET_PROMOS] (state, payload) {
    state.promos = payload
  },
  [types.SEARCH_PROMOS] (state, payload) {
    state.searchedPromos = payload
  }
}