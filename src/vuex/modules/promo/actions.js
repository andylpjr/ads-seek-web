import axios from '../../../apis'

export default {
    fetchPromos(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of promos
                axios.get(payload.url)
                .then(response => {
                    if(response.status === 200) { 
                        context.commit('SET_PROMOS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    fetchPromoById(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of promos by Id
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_PROMOS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    // Create or Update promo information
    savePromos(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 201 || response.status === 200) {  
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    // Delete promo information
    deletePromo(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 200) { 
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    autocompletePromos(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SEARCH_PROMOS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    }
}