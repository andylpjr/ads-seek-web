import types from '../../mutation-types'

export default {
  [types.SET_CUSTOMERS] (state, payload) {
    state.customers = payload
  },
  [types.SEARCH_CUSTOMERS] (state, payload) {
    state.searchedCustomers = payload
  }
}