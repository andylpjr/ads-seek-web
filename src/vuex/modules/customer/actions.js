import axios from '../../../apis'

export default {
    fetchCustomers(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of customers
                axios.get(payload.url)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_CUSTOMERS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    fetchCustomerById(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of customers by Id
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_CUSTOMERS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    // Create or Update customer information
    saveCustomers(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 201 || response.status === 200) {  
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    // Delete customer information
    deleteCustomer(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 200) { 
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    autocompleteCustomers(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SEARCH_CUSTOMERS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    }
}