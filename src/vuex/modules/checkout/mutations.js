import types from '../../mutation-types'

export default {
  [types.SET_CHECKOUTS] (state, payload) {
    state.checkouts = payload
  },
}