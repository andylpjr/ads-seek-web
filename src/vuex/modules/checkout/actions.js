import axios from '../../../apis'

export default {
    fetchCheckouts(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of Checkouts
                axios.get(payload.url)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_CHECKOUTS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    fetchCheckoutById(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // retrieve data of customers by Id
                axios.get(payload.url, payload.query)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_CHECKOUTS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    },
    // Confirm Checkout information
    saveCheckouts(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    context.dispatch('setNotification', response, { root: true })
                    if(response.status === 201 || response.status === 200) {  
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 500);
        })
    },
    // Create Checkout information
    createCheckout(context, payload) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                axios.post(payload.url, payload.data)
                .then(response => {
                    if(response.status === 200) {
                        context.commit('SET_CHECKOUTS', response.data.data)
                        resolve(true)
                    } else {
                        reject(false)
                    }
                })
            }, 0);
        })
    }
}