import Sidebar from '../components/Sidebar.vue'
import Home from '../views/Home.vue'
import Ad from '../views/Ad.vue'
import AdDetail from '../views/AdDetail.vue'
import Promo from '../views/Promo.vue'
import PromoDetail from '../views/PromoDetail.vue'
import Customer from '../views/Customer.vue'
import CustomerDetail from '../views/CustomerDetail.vue'
import Checkout from '../views/Checkout.vue'
import CheckoutPackage from '../views/CheckoutPackage.vue'
import CheckoutDetail from '../views/CheckoutDetail.vue'

const routes = [
    {
        path: '/',
        components: {
            sidebar: Sidebar,
            content: Home
        },
        name: 'main'
    },
    {
        path: '/ad',
        components: {
            sidebar: Sidebar,
            content: Ad
        },
        name: 'ad'
    },
    {
        path: '/ad-detail',
        components: {
            sidebar: Sidebar,
            content: AdDetail
        },
        name: 'adDetail'
    },
    {
        path: '/promo',
        components: {
            sidebar: Sidebar,
            content: Promo
        },
        name: 'promo'
    },
    {
        path: '/promo-detail',
        components: {
            sidebar: Sidebar,
            content: PromoDetail
        },
        name: 'promoDetail'
    },
    {
        path: '/customer',
        components: {
            sidebar: Sidebar,
            content: Customer
        },
        name: 'customer'
    },
    {
        path: '/customer-detail',
        components: {
            sidebar: Sidebar,
            content: CustomerDetail
        },
        name: 'customerDetail'
    },
    {
        path: '/checkout',
        components: {
            sidebar: Sidebar,
            content: Checkout
        },
        name: 'checkout'
    },
    {
        path: '/checkout-package',
        components: {
            sidebar: Sidebar,
            content: CheckoutPackage
        },
        name: 'checkoutPackage'
    },
    {
        path: '/checkout-detail',
        components: {
            sidebar: Sidebar,
            content: CheckoutDetail
        },
        name: 'checkoutDetail'
    },
]

export default routes