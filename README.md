# SEEK-web

> SEEK Ads - Web

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
# for server deployment change webpack.config.js public path to ./dist/
# for development mode change webpack.config.js public path to /dist/
# index.html point to build.js

### DOCUMENTATION
Frontend Structure consist of Vue.js & Bootstrap

App Structure -> {
    apis: config for axios to connect to API
    assets: all assets data
    components: apps reuseable components
    router: config all route of apps
    views: view for every module / pages
    vuex: store framework of applications
}
